#language:pt

@pedido @regressao
Funcionalidade: Consultar o ultimo pedido

@login @sair_impersonate
Cenario: consulta detalhe do ultimo pedido
Dado que eu acesse o admin e consulte os pedidos
Quando eu clicar no ultimo pedidos
Entao entao poderei ver os detalhes e status do ultimo pedido feito