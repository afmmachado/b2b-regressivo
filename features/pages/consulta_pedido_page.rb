#encoding: utf-8

class Admin < SitePrism::Page

element :menu_vendas, 'body > div.wrapper > div.main-sidebar > div > ul > li:nth-child(3) > a'
element :menu_pedidos, 'body > div.wrapper > div.main-sidebar > div > ul > li.treeview.active > ul > li:nth-child(1) > a'
element :btn_verpedido, '#orders-grid > table > tbody > tr:nth-child(1) > td:nth-child(10) > a > i'
element :campo_email_pedido, '#tab-info > div > div:nth-child(2) > div > div:nth-child(1) > div.col-md-9 > a'
element :aba_impersonate, '#customer-edit > ul > li:nth-child(7) > a'
element :btn_fazerpedido, '#tab-impersonate > div > div > div > button'

#elementos abaixo são na pagina de pedidos do site apos o impersonate
element :btn_pesquisa_pedido, '#form-orders > div.fieldset > div.form-fields > div.form-submit > input'    
element :btn_detalhe_pedido, '#order-list > div > fieldset > div > div > table > tbody > tr:nth-child(1) > td:nth-child(8) > a'


def admin_ultimo_pedido
    sleep 7
    menu_vendas.click
    sleep 2
    menu_pedidos.click
    sleep 4
    btn_verpedido.click
    sleep 4
    campo_email_pedido.click
    sleep 7
    aba_impersonate.click
    sleep 4
    btn_fazerpedido.click
    sleep 12


end    




end    