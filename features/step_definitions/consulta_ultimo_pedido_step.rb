  Dado("que eu acesse o admin e consulte os pedidos") do
    visit  '/admin'
    admin.admin_ultimo_pedido
  end
  
  Quando("eu clicar no ultimo pedidos") do
    visit '/Servicos/MeusPedidos'
    admin.btn_pesquisa_pedido.click
    sleep 4
    admin.btn_detalhe_pedido.click
    sleep 8
  end
  
  Entao("entao poderei ver os detalhes e status do ultimo pedido feito") do
   expect(page).to have_content 'INFORMAÇÕES DO PEDIDO'

   expect(page).to have_no_content 'Ooops, não encontramos o que você buscava! :('
  end