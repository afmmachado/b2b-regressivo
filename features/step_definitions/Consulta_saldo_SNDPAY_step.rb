

  Dado("que eu tenha um novo cliente") do
      snd_pay.load
        
  end
  
  Quando("eu faturar no carrinho de compras para este CNPJ com cond.pagamento SNDPAY") do
    snd_pay.cadastrar_revenda_add_produto_checkout

  end
  
  Entao("pedido sera  finalizado e uma solicitação de credito sera enviada") do
    expect(page).to have_content 'Obrigado, pedido finalizado com sucesso!'
  expect(page).to have_content 'Número Pedido:'
  expect(page).to have_content 'CONCLUSÃO'
  expect(page).to have_content CONFIG['produto1']
  find('input[value="Continue Comprando"]')
  expect(page).to have_xpath("//img[@src='/Themes/Pavilion/Content/images/seloabcomn.png']")
  end